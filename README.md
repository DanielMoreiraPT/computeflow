# ComputeFlow
ComputeFlow is a project with the goal of developing software able to provide an intuitive and interactive Interface for users that want to design flow-based programming programs.

## About
ComputeFlow was a final project for Informatics Engineering Bachelor's degree and this is our personal continuation of its development.

## What is different
Our goal is to perform a complete refractoring of the original project and build up from a strong fundation.